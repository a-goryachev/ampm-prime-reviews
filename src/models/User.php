<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "User".
 *
 * @property integer $id
 * @property string $email
 * @property string $username - virtual property returns email
 * @property integer $type
 * @property string $auth_key
 * @property string $access_token
 * @property integer $access_token_expires
 * @property string $activation_token
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $status
 * @property integer $created_at
 * @property integer $activated_at
 */
class User extends ActiveRecord implements IdentityInterface
{
    const TYPE_ADMINISTRATOR = 1;
    const TYPE_REVIEWER = 2;
    const TYPE_SELLER = 3;

    const STATUS_REGISTERED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_BLOCKED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{User}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'type', 'auth_key', 'activation_token', 'access_token', 'access_token_expires', 'password_hash'], 'required'],
            [['type', 'access_token_expires', 'status', 'created_at', 'activated_at'], 'integer'],
            ['type', 'in', 'range' => [self::TYPE_ADMINISTRATOR, self::TYPE_REVIEWER, self::TYPE_SELLER]],
            ['status', 'in', 'range' => [self::STATUS_REGISTERED, self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_BLOCKED]],
            ['status', 'default', 'value' => self::STATUS_REGISTERED],
            [['email', 'access_token', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            ['email', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'type' => 'Type',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
            'access_token_expires' => 'Access Token Expires',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'status' => 'Status',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::find()->where('id = :id', [':id' => $id])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where('access_token = :token', [':token' => $token])->one();
    }

    /**
     * Finds user by username
     *
     * @param  string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::find()->where('email = :username', [':username' => $username])->one();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = \Yii::$app->security->generateRandomString(64) . time();
        $this->save();
    }

    public function setPassword($password)
    {
        $this->password_hash = \Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function beforeValidate()
    {
        $security = \Yii::$app->security;
        if ($this->isNewRecord) {
            $this->created_at = $this->created_at ?: time();
            $this->auth_key = $this->auth_key ?: $security->generateRandomString(32);
            $this->access_token_expires = 60 * 60 * 24 * 30;
            $this->access_token = $this->access_token ?: $security->generateRandomString(64) . time();
            $this->activation_token = $this->activation_token ?: $security->generateRandomString(64) . time();
            $this->password_reset_token = $this->password_reset_token ?: $security->generateRandomString(64) . time();
            $this->password_hash = $this->password_hash ?: $security->generatePasswordHash($security->generateRandomString(32) . time());
        }
        return parent::beforeValidate();
    }

    public function getUsername()
    {
        return $this->email;
    }
}
