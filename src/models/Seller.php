<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Seller".
 *
 * @property integer $id
 * @property string $full_name
 * @property integer $user_id
 * @property integer $status
 *
 * @property User $user
 */
class Seller extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{Seller}}';
    }

    /**
     * @param string $email
     * @return array|null|Reviewer
     */
    public static function findByEmail($email)
    {
        $reviewer = self::find()
            ->joinWith('user')
            ->where('"User".email = :email', [':email' => $email])
            ->andWhere('"User".type = :type', [':type' => User::TYPE_SELLER])
            ->one();
        return $reviewer;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'user_id'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['full_name'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'user_id' => 'User ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
