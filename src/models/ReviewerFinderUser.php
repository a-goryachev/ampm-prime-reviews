<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ReviewerFinderUser".
 *
 * @property integer $id
 * @property string $email
 * @property string $activationCode
 * @property integer $numberOfActivations
 * @property integer $created_at
 * @property string $name
 * @property string $token
 * @property integer $ac_subscription_status
 * @property integer $ac_subscriber_id
 */
class ReviewerFinderUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ReviewerFinderUser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['numberOfActivations', 'created_at', 'ac_subscription_status', 'ac_subscriber_id'], 'integer'],
            [['email', 'name'], 'string', 'max' => 255],
            [['activationCode'], 'string', 'max' => 6],
            [['token'], 'string', 'max' => 128],
            [['email'],'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'activationCode' => 'Activation Code',
            'numberOfActivations' => 'Number Of Activations',
            'created_at' => 'Created At',
            'name' => 'Name',
            'token' => 'Token',
            'ac_subscription_status' => 'Ac Subscription Status',
            'ac_subscriber_id' => 'Ac Subscriber ID',
        ];
    }

    public function generateActivationCode()
    {
        $this->activationCode = \Yii::$app->security->generateRandomString(6);
        $this->save();
    }

    public function generateToken()
    {
        $this->token = \Yii::$app->security->generateRandomString(128);
        $this->save();
    }

}
