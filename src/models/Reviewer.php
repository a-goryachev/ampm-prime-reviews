<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "Reviewer".
 *
 * @property integer $id
 * @property string $amazon_profile_url
 * @property integer $user_id
 * @property integer $status
 * @property string $phone
 *
 * @property User $user
 */
class Reviewer extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{Reviewer}}';
    }

    /**
     * @param string $email
     * @return array|null|Reviewer
     */
    public static function findByEmail($email)
    {
        $reviewer = self::find()
            ->joinWith('user')
            ->where('"User".email = :email', [':email' => $email])
            ->andWhere('"User".type = :type', [':type' => User::TYPE_REVIEWER])
            ->one();
        return $reviewer;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amazon_profile_url', 'user_id', 'phone'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['amazon_profile_url'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amazon_profile_url' => 'Amazon Profile Url',
            'user_id' => 'User ID',
            'status' => 'Status',
            'phone' => 'Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
