<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ReviewerFinderUser".
 *
 * @property integer $id
 * @property string $email
 * @property string $activationCode
 * @property integer $numberOfActivations
 * @property integer $created_at
 * @property string $name
 * @property string $token
 * @property integer $subscription_status
 */
class RevieweFinderUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ReviewerFinderUser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['numberOfActivations', 'created_at', 'subscription_status'], 'integer'],
            [['email', 'name'], 'string', 'max' => 255],
            [['activationCode'], 'string', 'max' => 6],
            [['token'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'activationCode' => 'Activation Code',
            'numberOfActivations' => 'Number Of Activations',
            'created_at' => 'Created At',
            'name' => 'Name',
            'token' => 'Token',
            'subscription_status' => 'Subscription Status',
        ];
    }
}
