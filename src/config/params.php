<?php

return [
    'adminEmail' => 'alexfoxlost@mail.ru',
    'emailTemplates' => [
        'reviewer_activation_email' => [
            'subject' => 'Please finish signup process',
            'view' => '@app/modules/reviewer/views/emails/signup/activation'
        ],
        'reviewer_password_recovery_email' => [
            'subject' => 'Password recovery',
            'view' => '@app/modules/reviewer/views/emails/auth/password-recovery'
        ],
        'seller_activation_email' => [
            'subject' => 'Please finish signup process',
            'view' => '@app/modules/seller/views/emails/signup/activation'
        ],
        'seller_password_recovery_email' => [
            'subject' => 'Password recovery',
            'view' => '@app/modules/seller/views/emails/auth/password-recovery'
        ],
        'reviewerFinder_activation_email' => [
            'subject' => 'Your ReviewRaid Activation Code Inside',
            'view' => '@app/modules/io/views/emails/reviewer-finder/activation',
            'from' => ['noreply@ampmpodcast.com' => 'AM/PM Podcast']
        ],
    ],
    'reviewer_activation_url' => 'http://prime-reviews.dev.agoryachev.ru/reviewer/#/account-activation',
    'reviewer_password_recovery_url' => 'http://prime-reviews.dev.agoryachev.ru/reviewer/#/password-recovery',
    'seller_activation_url' => 'http://prime-reviews.dev.agoryachev.ru/seller/#/account-activation',
    'seller_password_recovery_url' => 'http://prime-reviews.dev.agoryachev.ru/seller/#/password-recovery',
    'reviewer_finder_users_active_campaign_list_id'=>2
];
