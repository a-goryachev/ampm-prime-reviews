<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
    'controllerMap' => [
        'migrate' => [
            'class' => 'app\commands\MigrateController'
        ]
    ],
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'ac' => [
            'class' => 'app\components\ActiveCampaign',
            'url' => 'https://ampm.api-us1.com',
//            'url' => 'https://goryachevgroup.api-us1.com',
            'key' => 'fccc4ed7add1dd7386af92910ea026f409b672e40fc07d6dfe42fb9beef9d7fb0547cb62'
//            'key' => '919a91cdaae59adb47c5918eb3a4a4101de2265c0d709c0984938c6dc7b9c3438bd17da9'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];
