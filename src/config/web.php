<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'BA4J2cKa4F8uaUowJepYzSmGjhj8re8K',
            'enableCookieValidation' => true,
        ],
        'response' => [
            'charset' => 'UTF-8',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\MemCache',
            'servers' => [
                [
                    'host' => 'memcached',
                    'port' => 11211,
                    'weight' => 60,
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableSession' => true
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'ac' => [
            'class' => 'app\components\ActiveCampaign',
            'url' => 'https://ampm.api-us1.com',
//            'url' => 'https://goryachevgroup.api-us1.com',
            'key' => 'fccc4ed7add1dd7386af92910ea026f409b672e40fc07d6dfe42fb9beef9d7fb0547cb62'
//            'key' => '919a91cdaae59adb47c5918eb3a4a4101de2265c0d709c0984938c6dc7b9c3438bd17da9'
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
            'viewPath' => '@app/mail',
            'useFileTransport' => false,
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['noreply@prime-reviews.com' => 'Prime Reviews'],
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mandrillapp.com',
                'username' => 'alexfoxlost@mail.ru',
                'password' => 'LtIAIdYDxLEqlr1_YeF-1A',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'modules' => [
        'reviewer' => [
            'class' => 'app\modules\reviewer\Module',
        ],
        'seller' => [
            'class' => 'app\modules\seller\Module',
        ],
        'io' => [
            'class' => 'app\modules\io\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.*', '172.17.*']
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.*', '172.17.*']
    ];
}

return $config;
