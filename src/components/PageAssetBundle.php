<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PageAssetBundle extends AssetBundle
{
    public $publishOptions = [
        'forceCopy' => false
    ];
    public $js = [
        'page.js'
    ];
    public $css = [
        'page.css'
    ];
    public $jsOptions = [
        'position' => View::POS_END
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public function init()
    {
        $obj = new \ReflectionClass($this);
        $filename = $obj->getFileName();
        $this->sourcePath = dirname($filename) . DIRECTORY_SEPARATOR . 'assets';
    }
}
