<?php

namespace app\components;


use yii\base\Component;
use yii\base\InvalidConfigException;

class ActiveCampaign extends Component
{
    public $url;
    public $key;

    public $instance;

    public function init()
    {
        $this->instance = new \ActiveCampaign($this->url, $this->key);
        if (!(int)$this->instance->credentials_test()) {
            throw new InvalidConfigException('Invalid Active Campaign credentials.');
        }
    }
}