<?php
namespace app\commands;

use app\models\ReviewerFinderUser;
use yii\console\Controller;

/**
 *
 */
class CronController extends Controller
{
    /**
     * Synchronize subscription status from Active Campaign to local database
     */
    public function actionUpdateReviewerFinderUserSubscriptionStatus()
    {
        /* Get list from Active Campaign*/
        $ac = \Yii::$app->ac->instance;
        /* @var \ActiveCampaign $ac */
        $listId = \Yii::$app->params['reviewer_finder_users_active_campaign_list_id'];
        $ac->version(1);
        $contacts_view = $ac->api("contact/list?filters[listid]={$listId}&page=all");
        print_r($contacts_view);
        foreach($contacts_view as $contact){
            if(!isset($contact->subscriberid)){
                continue;
            }
            /* @var \app\models\ReviewerFinderUser $user */
            $user=ReviewerFinderUser::findOne(['ac_subscriber_id'=>$contact->subscriberid]);
            if($user!==null){
                $user->ac_subscription_status=$contact->status;
                $user->save();
                $this->stdout('User '.$user->email.' updated'.PHP_EOL);
            }
        }
    }
}
