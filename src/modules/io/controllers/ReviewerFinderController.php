<?php
namespace app\modules\io\controllers;

use app\models\ReviewerFinderUser;
use yii\web\Controller;
use yii\web\Response;

class ReviewerFinderController extends Controller
{

    public $enableCsrfValidation = false;

    public function actionRequestCode()
    {
        \Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        $name = \Yii::$app->getRequest()->post('name');
        $email = \Yii::$app->getRequest()->post('email');

        $model = ReviewerFinderUser::findOne(['email' => $email]);
        if ($model === null) {
            $model = new ReviewerFinderUser();
            $model->email = $email;
            $model->created_at = time();
            $model->generateToken();
            $model->ac_subscription_status = 0;
        }
        $model->name = $name;

        if (!$model->validate()||!$model->save()) {
            return ['err' => $model->getErrors()];
        }

        $model->generateActivationCode();

        if (!$this->sendActivationCodeToEmail($model)) {
            return ['err' => 'Can not send activation code. Please try later.'];
        }

        return ['err' => false];
    }

    public function actionActivate()
    {
        \Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $email = \Yii::$app->getRequest()->post('email');
        $code = \Yii::$app->getRequest()->post('code');

        $user = ReviewerFinderUser::findOne(['email' => $email]);
        /* @var ReviewerFinderUser $user */

        if ($user !== null && $user->activationCode === $code) {
            $user->numberOfActivations++;
            $user->save();

            /* Add email to Active Campaign list */
            $ac = \Yii::$app->ac->instance;
            /* @var \ActiveCampaign $ac */
            $listId = \Yii::$app->params['reviewer_finder_users_active_campaign_list_id'];
            $contact = array(
                "email" => $user->email,
                "name" => $user->name,
                "p[{$listId}]" => $listId,
                "status[1]" => 1,//active
            );

            $api_response=$ac->api("contact/sync", $contact);
            if ((int)$api_response->result_code) {
                // successful request
                $user->ac_subscriber_id = (int)$api_response->subscriber_id;
                $user->ac_subscription_status=1;
                $user->save();
            }

            return [
                'err' => [],
                'email' => $user->email,
                'token' => $user->token,
                'subscribed' => $user->ac_subscription_status
            ];
        }

        return ['err' => ['activationCode'=>'Invalid activation code.']];
    }

    public function actionSubscribe()
    {
        \Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $token = \Yii::$app->getRequest()->post('token');

        $user = ReviewerFinderUser::findOne(['token' => $token]);
        /* @var ReviewerFinderUser $user */

        if ($user !== null ) {
            /* Add email to Active Campaign list */
            $ac = \Yii::$app->ac->instance;
            /* @var \ActiveCampaign $ac */
            $listId = \Yii::$app->params['reviewer_finder_users_active_campaign_list_id'];
            $contact = array(
                "email" => $user->email,
                "name" => $user->name,
                "p[{$listId}]" => $listId,
                "status[1]" => 1,//active
            );

            $api_response=$ac->api("contact/sync", $contact);
            if ((int)$api_response->result_code) {
                // successful request
                $user->ac_subscriber_id = (int)$api_response->subscriber_id;
                $user->ac_subscription_status=1;
                $user->save();
            }

            return [
                'err' => false,
                'subscribed' => $user->ac_subscription_status
            ];
        }

        return ['err' => true];
    }

    public function actionGetSubscriptionStatus()
    {
        \Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $token = \Yii::$app->getRequest()->post('token');

        $user = ReviewerFinderUser::findOne(['token' => $token]);
        /* @var ReviewerFinderUser $user */

        if ($user !== null ) {
            return [
                'err' => false,
                'subscribed' => $user->ac_subscription_status
            ];
        }

        return ['err' => true];
    }

    protected function sendActivationCodeToEmail($user)
    {
        if (!isset(\Yii::$app->params['emailTemplates'])) {
            throw new InvalidConfigException('Email templates not configured');
        }

        if (!isset(\Yii::$app->params['emailTemplates']['reviewerFinder_activation_email'])) {
            throw new InvalidConfigException('Email template not found');
        }

        $template = \Yii::$app->params['emailTemplates']['reviewerFinder_activation_email'];
        $message = \Yii::$app->mailer->compose($template['view'], [
                'model' => $user,
            ]
        );
        $message->setSubject($template['subject']);
        $message->setTo($user->email);
        if (isset(\Yii::$app->params['emailTemplates']['reviewerFinder_activation_email']['from'])) {
            $message->setFrom(\Yii::$app->params['emailTemplates']['reviewerFinder_activation_email']['from']);
        }
        return $message->send();
    }

}