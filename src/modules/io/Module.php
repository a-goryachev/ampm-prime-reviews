<?php

namespace app\modules\io;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\io\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
