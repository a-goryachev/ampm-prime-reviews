<?php
/**
 * @var app\models\ReviewerFinderUser $model
 */
?>
<p>Hello, <?=$model->name?>!</p>
<p>Congratulations! You are just one step away from finding hundreds of reviewers for your Amazon products.</p>
<p>Please copy the activation code below into the activation code field on your ReviewRaid tool.</p>
<p><strong>Activation code: </strong><?=$model->activationCode?></p>
<p>
    ReviewRaid is just one of the tools you get access to for being an active AM/PM Podcast subscriber.
    To learn more about the AM/PM Podcast visit our website at <a href="http://www.AMPMPodcast.com">www.AMPMPodcast.com</a>.
</p>
<p>
    We also have a fantastic Facebook community over at <a href="http://www.facebook.com/groups/ampmpodcast">http://www.facebook.com/groups/ampmpodcast</a> we hope to see you there!
</p>
<p>Sincerely,</p>
<p>The AM/PM Podcast Team<br>
<a href="http://www.AMPMPodcast.com">http://www.AMPMPodcast.com</a><br>
<a href="http://www.facebook.com/groups/ampmpodcast">http://www.facebook.com/groups/ampmpodcast</a>
</p>
