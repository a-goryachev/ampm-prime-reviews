<?php
$um = \Yii::$app->urlManager;
?>
<nav role="navigation">
    <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
    <div class="small-logo">
        <span class="text-primary">PRIME REVIEWS</span>
    </div>
    <form role="search" class="navbar-form-custom" method="post" action="#">
        <div class="form-group">
            <input type="text" placeholder="Search something special" class="form-control" name="search">
        </div>
    </form>
    <div class="mobile-menu">
        <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse"
                data-target="#mobile-collapse">
            <i class="fa fa-chevron-down"></i>
        </button>
        <div class="collapse mobile-navbar" id="mobile-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a class="" href="<?= $um->createUrl('/seller/profile/index') ?>">Profile</a>
                </li>
                <li>
                    <a class="" href="<?= $um->createUrl('/seller/auth/logout') ?>">Logout</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="navbar-right">
        <ul class="nav navbar-nav no-borders">
            <?= $this->render('_notifications') ?>
            <?= $this->render('_bigMenu') ?>
            <?= $this->render('_messagesBtn') ?>
            <li>
                <a href="#" id="sidebar" class="right-sidebar-toggle">
                    <i class="pe-7s-upload pe-7s-news-paper"></i>
                </a>
            </li>
            <li class="dropdown">
                <a href="<?= $um->createUrl('/seller/auth/logout') ?>">
                    <i class="pe-7s-upload pe-rotate-90"></i>
                </a>
            </li>
        </ul>
    </div>
</nav>