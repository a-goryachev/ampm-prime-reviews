<?php
use app\modules\seller\models\SignupForm;
use yii\widgets\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\modules\seller\models\SignupForm $model
 */
?>
<div class="register-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h1>Join Prime Reviews</h1>
                <small>Seller account required to get best reviews for your products.</small>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'signup-form',
                        'errorCssClass' => 'has-error',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => false,
                        'validateOnBlur' => false,
                        'validateOnChange' => false,
                        'options' => [],
                        'fieldConfig' => [
                            'template' => "{input}{error}",
                            'selectors' => [
                                'error' => '.error-text',
                            ],
                            'errorOptions' => [
                                'tag' => 'label',
                                'class' => 'error-text',
                                'style' => 'min-height: 12px; font-size: 12px;',
                            ],
                            'inputOptions' => ['class' => 'form-control'],
                        ],
                    ]); ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="text-bold text-center text-uppercase">
                                Fill the form below to complete your registration
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'name', [
                                'inputOptions' => [
                                    'placeholder' => 'Your full name'
                                ],
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'email', [
                                'inputOptions' => [
                                    'placeholder' => 'Email'
                                ],
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($model, 'password', [
                                'inputOptions' => [
                                    'placeholder' => 'Password'
                                ],
                            ])->passwordInput() ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'password_repeat', [
                                'inputOptions' => [
                                    'placeholder' => 'Repeat password'
                                ],
                            ])->passwordInput() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <?php ob_start(); ?>
                            <div class="checkbox" style="margin-bottom: 0px;">
                                <label style="padding-left: 0px;"> {input} I agree with <a href="#">Terms and
                                        Conditions</a> </label>
                            </div>
                            {error}
                            <?php $template = ob_get_clean(); ?>
                            <?= $form->field($model, 'agree', [
                                'inputOptions' => [
                                    'class' => 'i-checks',
                                ],
                                'template' => $template,
                            ])->checkbox(['class' => 'i-checks'], false) ?>
                        </div>
                        <div class="col-sm-4">
                            <?php if ($model->scenario == SignupForm::CAPTCHA_REQUIRED): ?>
                                <?php ob_start(); ?>
                                <div class="input-group">
                                    {input}
                                    <div class="input-group-addon" style="padding: 0px;">{image}</div>
                                </div>
                                <?php $template = ob_get_clean(); ?>
                                <?= $form->field($model, 'captcha', [
                                ])->widget(\yii\captcha\Captcha::classname(), [
                                    'captchaAction' => '/site/captcha',
                                    'options' => [
                                        'autocomplete' => 'off',
                                        'placeholder' => 'Type symbols',
                                        'class' => 'form-control'
                                    ],
                                    'imageOptions' => [
                                        'style' => 'height: 34px; width: auto;',
                                    ],
                                    'template' => $template,
                                ]) ?>
                            <?php endif; ?>

                        </div>
                        <div class="col-sm-2">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success"><strong>Join</strong></button>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <strong>Prime Reviews</strong> - The best way to bind sellers and reviewers <br/> 2015 Copyright AM/PM
        </div>
    </div>
</div>
