<?php
/**
 * @var string $activationUrl
 */
?>
<p>Please activate your account: <a href="<?= $activationUrl ?>"><?= $activationUrl ?></a></p>