<?php
/**
 * @var string $passwordRecoveryUrl
 */
?>
<p>Follow this link to set new password: <a href="<?= $passwordRecoveryUrl ?>"><?= $passwordRecoveryUrl ?></a></p>