<?php
use yii\widgets\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var \app\modules\seller\models\PasswordRecoveryForm $model
 */
?>
<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>PRIME REVIEWS</h3>
                <small>Password recovery</small>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'password-recovery-form',
                        'errorCssClass' => 'has-error',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => false,
                        'validateOnBlur' => false,
                        'validateOnChange' => false,
                        'options' => [],
                        'fieldConfig' => [
                            'template' => "{label}{input}{error}",
                            'selectors' => [
                                'error' => '.error-text',
                            ],
                            'options' => [
                                'style' => 'margin-bottom: 0px;'
                            ],
                            'labelOptions' => [
                                'class' => 'control-label-t'
                            ],
                            'errorOptions' => [
                                'tag' => 'label',
                                'class' => 'error-text',
                                'style' => 'min-height: 12px; font-size: 12px;',
                            ],
                            'inputOptions' => ['class' => 'form-control'],
                        ],
                    ]); ?>

                    <?= $form->field($model, 'email', [
                        'inputOptions' => [
                            'placeholder' => 'Please enter your e-mail',
                        ],
                    ]) ?>
                    <button type="submit" class="btn btn-success btn-block">Recovery password</button>
                    <a class="btn btn-default btn-block"
                       href="<?= \Yii::$app->urlManager->createUrl('/seller/auth/login') ?>">Login</a>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <strong>Prime Reviews</strong> - The best way to bind sellers and reviewers <br/> 2015 Copyright AM/PM
        </div>
    </div>
</div>
