<?php
namespace app\modules\seller\assets;

use yii\web\AssetBundle;
use yii\web\View;

class MainAsset extends AssetBundle
{
    public $publishOptions = [
        'forceCopy' => false
    ];
    public $sourcePath = '@app/.design/homer/Homer_Full_Version_HTML_JS';
    public $css = [
        /* Vendor styles */
        'vendor/fontawesome/css/font-awesome.css',
        'vendor/metisMenu/dist/metisMenu.css',
        'vendor/animate.css/animate.css',
        'vendor/bootstrap/dist/css/bootstrap.css',
        'vendor/toastr/build/toastr.min.css',
        /* App styles */
        'fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css',
        'fonts/pe-icon-7-stroke/css/helper.css',
        'styles/style.css',
    ];
    public $js = [
        /* Vendor scripts */
        'vendor/slimScroll/jquery.slimscroll.min.js',
        'vendor/bootstrap/dist/js/bootstrap.min.js',
        'vendor/metisMenu/dist/metisMenu.min.js',
        'vendor/iCheck/icheck.min.js',
        'vendor/sparkline/index.js',
        'vendor/toastr/build/toastr.min.js',
        /* App scripts */
        'scripts/homer.js',
    ];
    public $jsOptions = [
        'position' => View::POS_END
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset'
    ];
}