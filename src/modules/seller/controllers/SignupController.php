<?php

namespace app\modules\seller\controllers;

use app\models\Seller;
use app\models\User;
use app\modules\seller\components\BaseController;
use app\modules\seller\models\SignUpForm;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;

class SignupController extends BaseController
{
    public $layout = 'noauth';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return ArrayHelper::merge($behaviors, [
            'access' => [
                'rules' => [
                    [
                        'actions' => ['index', 'complete', 'activate'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'activate' => ['get'],
                    'complete' => ['get'],
                ],
            ],
        ]);
    }

    public function actionComplete()
    {
        return $this->render('complete');
    }

    public function actionIndex()
    {

        $signupAttempts = \Yii::$app->session->get('signup_attempts', 0);

        $model = new SignUpForm();
        if ($signupAttempts > 2) {
            $model->scenario = SignupForm::CAPTCHA_REQUIRED;
        }

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $user = new User();
            $user->email = $model->email;
            $user->type = User::TYPE_SELLER;
            $user->status = User::STATUS_REGISTERED;
            $user->password_hash = \Yii::$app->security->generatePasswordHash($model->password);

            if (!$user->save()) {
                throw new ServerErrorHttpException('Can not create user.');
            }

            /* Create Seller */
            $seller = new Seller();
            $seller->user_id = $user->id;
            $seller->full_name = $model->name;

            if (!$seller->save()) {
                throw new ServerErrorHttpException('Can not create seller profile.');
            }

            if (!$this->sendActivationEmail($user)) {
                throw new ServerErrorHttpException('Can not send email message');
            }
            return $this->redirect(\Yii::$app->urlManager->createUrl('/seller/signup/complete'));
        }

        if ($model->hasErrors()) {
            $signupAttempts++;
            \Yii::$app->session->set('signup_attempts', $signupAttempts);
            if ($signupAttempts > 2) {
                $model->scenario = SignupForm::CAPTCHA_REQUIRED;
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionActivate($key)
    {
        /**
         * @var User $user
         */
        $user = User::find()->where('activation_token = :key', [':key' => $key])->one();

        if (!$user) {
            return $this->render('activation-failed', ['message' => 'Incorrect activation key']);
        }
        $user->activated_at = time();
        $user->status = User::STATUS_ACTIVE;
        if (!$user->save()) {
            return $this->render('activation-failed', ['message' => 'For technical reasons, we can not activate your account right now. Please try later.']);
        }
        \Yii::$app->session->addFlash('success', 'Your account successfully activated.');
        return $this->redirect('/seller/auth/login');
    }

    /**
     * @param User $user
     * @return bool
     * @throws InvalidConfigException
     */
    protected function sendActivationEmail($user)
    {
        if (!isset(\Yii::$app->params['emailTemplates'])) {
            throw new InvalidConfigException('Email templates not configured');
        }

        if (!isset(\Yii::$app->params['emailTemplates']['seller_activation_email'])) {
            throw new InvalidConfigException('Email template not found');
        }

        $template = \Yii::$app->params['emailTemplates']['seller_activation_email'];
        $message = \Yii::$app->mailer->compose($template['view'], [
                'activationUrl' => \Yii::$app->urlManager->createAbsoluteUrl(['/seller/signup/activate', 'key' => $user->activation_token]),
            ]
        );
        $message->setSubject($template['subject']);
        $message->setTo($user->email);
        return $message->send();
    }
}