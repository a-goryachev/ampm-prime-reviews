<?php

namespace app\modules\seller;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\seller\controllers';

    public function init()
    {
        parent::init();
    }

    public function beforeAction($action)
    {
        \Yii::$app->user->loginUrl = ['/seller/auth/login'];
        return parent::beforeAction($action);
    }
}
