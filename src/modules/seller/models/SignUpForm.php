<?php
namespace app\modules\seller\models;

use yii\base\Model;

/**
 * Class SignUpForm
 * @package app\modules\seller\models
 */
class SignUpForm extends Model
{
    const CAPTCHA_REQUIRED = 'strong_security';

    public $name;
    public $email;
    public $password;
    public $password_repeat;
    public $agree = false;
    public $captcha;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'email', 'password', 'password_repeat', 'agree'], 'required'],
            [['name'], 'string', 'max' => 500],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\models\User', 'targetAttribute' => 'email'],
            ['agree', 'boolean'],
            ['agree', 'validateAgree'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Password must be repeated exactly.'],
            ['captcha', 'captcha', 'captchaAction' => '/site/captcha', 'on' => self::CAPTCHA_REQUIRED],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'email' => 'Email',
            'agree' => 'I agree with Terms and Conditions',
            'password' => 'Password',
            'password_repeat' => 'Password confirmation',
            'captcha' => 'Please type this symbols',
        ];
    }

    public function validateAgree($attribute, $params)
    {
        if (!$this->agree) {
            $this->addError('agree', 'You must accept Terms and Conditions');
        }
    }

}