<?php

namespace app\modules\reviewer;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\reviewer\controllers';

    public function init()
    {
        parent::init();
    }

    public function beforeAction($action)
    {
        \Yii::$app->user->loginUrl = ['/reviewer/auth/login'];
        return parent::beforeAction($action);
    }
}
