<?php

/**
 * @var \yii\web\View $this
 * @var \app\modules\reviewer\models\PasswordRecoveryForm $model
 */
?>
<div class="register-container">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="alert">
                <p>
                    We sent a message to <?= $model->email ?>. Please check your mailbox and follow instructions.
                </p>
            </div>
        </div>
    </div>
</div>
