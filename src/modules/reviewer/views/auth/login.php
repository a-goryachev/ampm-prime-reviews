<?php
use app\modules\reviewer\models\LoginForm;
use yii\widgets\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var \app\modules\reviewer\models\LoginForm $model
 */
?>
<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>PLEASE LOGIN TO PRIME REVIEWS</h3>
                <small>This is the best app ever!</small>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'errorCssClass' => 'has-error',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => false,
                        'validateOnBlur' => false,
                        'validateOnChange' => false,
                        'options' => [],
                        'fieldConfig' => [
                            'template' => "{label}{input}{error}",
                            'selectors' => [
                                'error' => '.error-text',
                            ],
                            'options' => [
                                'style' => 'margin-bottom: 0px;'
                            ],
                            'labelOptions' => [
                                'class' => 'control-label-t'
                            ],
                            'errorOptions' => [
                                'tag' => 'label',
                                'class' => 'error-text',
                                'style' => 'min-height: 12px; font-size: 12px;',
                            ],
                            'inputOptions' => ['class' => 'form-control'],
                        ],
                    ]); ?>

                    <?= $form->field($model, 'username', [
                        'inputOptions' => [
                            'placeholder' => 'Please enter your e-mail',
                        ],
                    ]) ?>
                    <?= $form->field($model, 'password', [
                        'template' => '{label}<span style="font-size:10px; vertical-align: bottom;" class="pull-right"><a href="'
                            . \Yii::$app->urlManager->createUrl('/reviewer/password-recovery')
                            . '">Forgot password?</a></span>{input}{error}',
                        'inputOptions' => [
                            'placeholder' => 'Please enter your password',
                        ],
                    ])->passwordInput() ?>
                    <?php ob_start(); ?>
                    <div class="checkbox">
                        <label style="padding-left: 0px;"> {input} Remember me </label>

                        <p class="help-block small">(if this is a private computer)</p>
                    </div>
                    {error}
                    <?php $template = ob_get_clean(); ?>
                    <?= $form->field($model, 'rememberMe', [
                        'template' => $template,
                    ])->checkbox(['class' => 'i-checks'], false) ?>

                    <?php if ($model->scenario == LoginForm::CAPTCHA_REQUIRED): ?>
                        <?= $form->field($model, 'captcha', [
                            'options' => [
                                'autocomplete' => 'off'
                            ],
                        ])->widget(\yii\captcha\Captcha::classname(), [
                            'captchaAction' => '/site/captcha'
                        ]) ?>
                    <?php endif; ?>
                    <button type="submit" class="btn btn-success btn-block">Login</button>
                    <a class="btn btn-default btn-block"
                       href="<?= \Yii::$app->urlManager->createUrl('/reviewer/signup') ?>">Register</a>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <strong>Prime Reviews</strong> - The best way to bind sellers and reviewers <br/> 2015 Copyright AM/PM
        </div>
    </div>
</div>
