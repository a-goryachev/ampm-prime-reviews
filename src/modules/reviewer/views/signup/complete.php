<?php

/**
 * @var \yii\web\View $this
 */
?>
<div class="register-container">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="alert">
                <p>
                    Thank you for registration. Now please check mailbox and follow instructions to activate your
                    account.
                </p>
            </div>
        </div>
    </div>
</div>
