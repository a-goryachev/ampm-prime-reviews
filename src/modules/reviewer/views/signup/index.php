<?php
use app\modules\reviewer\assets\SignupAsset;
use yii\widgets\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\modules\reviewer\models\SignupForm $model
 */
SignupAsset::register($this);
?>
<div class="register-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h1>Join Prime Reviews</h1>
                <small>Reviewer account required to purchase discounted products or get it for free.</small>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'signup-form',
                        'errorCssClass' => 'has-error',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true,
                        'validateOnBlur' => false,
                        'validateOnChange' => false,
                        'options' => [],
                        'fieldConfig' => [
                            'template' => "{input}{error}",
                            'selectors' => [
                                'error' => '.error-text',
                            ],
                            'errorOptions' => [
                                'tag' => 'label',
                                'class' => 'error-text',
                                'style' => 'min-height: 12px; font-size: 12px;',
                            ],
                            'inputOptions' => ['class' => 'form-control'],
                        ],
                    ]); ?>
                    <?= $form->field($model, 'codeRefId')->hiddenInput() ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="text-bold text-center text-uppercase">
                                Enter your cell phone number to receive verification code
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 step1">
                            <?php ob_start(); ?>
                            <div class="input-group">
                                <label class="input-group-addon">+1</label>
                                {input}
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary action-send-code">Send code
                                        </button>
                                    </span>
                            </div>
                            {error}
                            <?php $template = ob_get_clean(); ?>
                            <?= $form->field($model, 'phone', [
                                'inputOptions' => [
                                    'placeholder' => 'Phone number'
                                ],
                                'template' => $template,
                            ])->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '999 999 9999',
                            ]) ?>
                        </div>
                        <div class="col-sm-2 text-center process-indicator">
                            <span style="line-height: 34px;" class="text-danger error hidden">Error</span>
                            <span style="line-height: 34px;" class="text-success success hidden">Code sent</span>
                                <span style="line-height: 34px;" class="spinner hidden">
                                    <i style="font-size: 34px;" class="fa fa-spinner fa-pulse"></i>
                                </span>
                        </div>
                        <div class="col-sm-4 step2">
                            <?= $form->field($model, 'code', [
                                'inputOptions' => [
                                    'placeholder' => 'Verification code'
                                ],
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <p>
                                SMS verification is nesassary for membership to prevent fraudulent accounts from
                                being set up.
                            </p>
                        </div>
                    </div>
                    <div class="row step2">
                        <div class="col-sm-12">
                            <h4 class="text-bold text-center text-uppercase">
                                Fill the form below to complete your registration
                            </h4>
                        </div>
                    </div>
                    <div class="row step2">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'email', [
                                'inputOptions' => [
                                    'placeholder' => 'Email'
                                ],
                            ]) ?>
                        </div>
                    </div>
                    <div class="row step2">
                        <div class="col-sm-6">
                            <?= $form->field($model, 'password', [
                                'inputOptions' => [
                                    'placeholder' => 'Password'
                                ],
                            ])->passwordInput() ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'password_repeat', [
                                'inputOptions' => [
                                    'placeholder' => 'Repeat password'
                                ],
                            ])->passwordInput() ?>
                        </div>
                    </div>
                    <div class="row step2">
                        <div class="col-sm-12">
                            <?php ob_start(); ?>
                            {input}
                            {error}
                            <span class="pull-right"><small><a href="#">How to get profile URL?</a></small></span>
                            <?php $template = ob_get_clean(); ?>
                            <?= $form->field($model, 'amazon_profile_url', [
                                'inputOptions' => [
                                    'placeholder' => 'Amazon Profile URL'
                                ],
                                'template' => $template,
                            ]) ?>
                        </div>
                    </div>
                    <div class="row step2">
                        <div class="col-sm-6">
                            <?php ob_start(); ?>
                            <div class="checkbox">
                                <label style="padding-left: 0px;"> {input} I agree with <a href="#">Terms and
                                        Conditions</a> </label>
                            </div>
                            {error}
                            <?php $template = ob_get_clean(); ?>
                            <?= $form->field($model, 'agree', [
                                'inputOptions' => [
                                    'class' => 'i-checks',
                                ],
                                'template' => $template,
                            ])->checkbox(['class' => 'i-checks'], false) ?>
                        </div>
                        <div class="col-sm-6">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success"><strong>Join</strong></button>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <strong>Prime Reviews</strong> - The best way to bind sellers and reviewers <br/> 2015 Copyright AM/PM
        </div>
    </div>
</div>
