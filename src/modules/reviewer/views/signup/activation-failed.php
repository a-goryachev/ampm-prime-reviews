<?php

/**
 * @var \yii\web\View $this
 * @var string $message
 */
?>
<div class="register-container">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <p>
                    <?= $message ?>
                </p>
            </div>
        </div>
    </div>
</div>
