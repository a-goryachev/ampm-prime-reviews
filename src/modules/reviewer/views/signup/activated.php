<?php

/**
 * @var \yii\web\View $this
 */
?>
<div class="register-container">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="alert">
                <p>
                    Your account activated. Please log in
                </p>
            </div>
        </div>
    </div>
</div>
