<?php
$um = \Yii::$app->urlManager;
?>
<div class="profile-picture">
    <a href="<?= $um->createUrl('/reviewer/default/index') ?>">
        <img src="http://lorempixel.com/200/200/people/" class="img-circle m-b" style="width: 76px; height: 76px;"
             alt="logo">
    </a>

    <div class="stats-label text-color">
        <span class="font-extra-bold font-uppercase"><?= \Yii::$app->user->identity->username ?></span>

        <div class="dropdown">
            <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                <small class="text-muted">Reviewer's bage <b class="caret"></b></small>
            </a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li><a href="<?= $um->createUrl('/reviewer/profile/index') ?>">Profile</a></li>
                <li class="divider"></li>
                <li><a href="<?= $um->createUrl('/reviewer/auth/logout') ?>">Logout</a></li>
            </ul>
        </div>
    </div>
</div>
