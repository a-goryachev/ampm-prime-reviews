<?php

namespace app\modules\reviewer\controllers;

use app\modules\reviewer\components\BaseController;
use app\modules\reviewer\models\LoginForm;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class AuthController extends BaseController
{
    public $layout = 'noauth';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return ArrayHelper::merge($behaviors, [
            'access' => [
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'login' => ['post', 'get'],
                    'logout' => ['get'],
                    'captcha' => ['get'],
                ],
            ],
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $loginAttempts = \Yii::$app->session->get('login_attempts', 0);

        $model = new LoginForm();
        if ($loginAttempts > 2) {
            $model->scenario = LoginForm::CAPTCHA_REQUIRED;
        }

        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            \Yii::$app->session->set('login_attempts', 0);
            return $this->goBack('/reviewer/default/index');
        }

        if ($model->hasErrors()) {
            $loginAttempts++;
            \Yii::$app->session->set('login_attempts', $loginAttempts);
            if ($loginAttempts > 2) {
                $model->scenario = LoginForm::CAPTCHA_REQUIRED;
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        \Yii::$app->user->logout();

        return $this->goHome();
    }

    public function goHome()
    {
        return $this->redirect('/reviewer/default/index');
    }

}