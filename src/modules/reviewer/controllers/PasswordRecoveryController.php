<?php

namespace app\modules\reviewer\controllers;

use app\models\Reviewer;
use app\models\User;
use app\modules\reviewer\components\BaseController;
use app\modules\reviewer\models\PasswordRecoveryForm;
use app\modules\reviewer\models\PasswordRecoverySetPasswordForm;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

class PasswordRecoveryController extends BaseController
{
    public $layout = 'noauth';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return ArrayHelper::merge($behaviors, [
            'access' => [
                'rules' => [
                    [
                        'actions' => ['index', 'set-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get', 'post'],
                    'set-password' => ['get', 'post'],
                ],
            ],
        ]);
    }

    public function actionIndex()
    {

        $model = new PasswordRecoveryForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {

            $reviewer = Reviewer::findByEmail($model->email);

            if ($reviewer && $reviewer->user) {
                $reviewer->user->generatePasswordResetToken();
                if (!$this->sendPasswordRecoveryEmail($reviewer->user)) {
                    throw new ServerErrorHttpException();
                }
            }
            return $this->render('token-sent', [
                'model' => $model
            ]);
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionSetPassword($key)
    {
        /**
         * @var User $user
         */
        $user = User::find()->where('password_reset_token = :key', [':key' => $key])->one();

        if (!$user) {
            throw new BadRequestHttpException("Invalid token.");
        }

        $model = new PasswordRecoverySetPasswordForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $user->setPassword($model->password);
            if (!$user->save()) {
                throw new ServerErrorHttpException('Can not update user password');
            }
            \Yii::$app->session->setFlash('success', 'Your password changed');
            return $this->redirect('/reviewer/auth/login');
        }
        return $this->render('set-password', [
            'model' => $model
        ]);
    }


    /**
     * @param User $user
     * @return bool
     * @throws InvalidConfigException
     */
    protected function sendPasswordRecoveryEmail($user)
    {
        if (!isset(\Yii::$app->params['emailTemplates'])) {
            throw new InvalidConfigException('Email templates not configured');
        }

        if (!isset(\Yii::$app->params['emailTemplates']['reviewer_password_recovery_email'])) {
            throw new InvalidConfigException('Email template not found');
        }

        $template = \Yii::$app->params['emailTemplates']['reviewer_password_recovery_email'];
        $message = \Yii::$app->mailer->compose($template['view'], [
                'passwordRecoveryUrl' => \Yii::$app->urlManager->createAbsoluteUrl(['/reviewer/password-recovery/set-password', 'key' => $user->password_reset_token]),
            ]
        );
        $message->setSubject($template['subject']);
        $message->setTo($user->email);
        return $message->send();
    }

}