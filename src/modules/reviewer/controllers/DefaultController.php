<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 16.12.15
 * Time: 14:45
 */

namespace app\modules\reviewer\controllers;

use app\modules\reviewer\components\BaseController;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class DefaultController extends BaseController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return ArrayHelper::merge($behaviors, [
            'access' => [
//                'rules' => [
//                    [
//                        'actions' => ['index'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                ],
            ],
        ]);
    }

    public function actionIndex()
    {
        return $this->renderContent("Empty dashboard");
    }
}