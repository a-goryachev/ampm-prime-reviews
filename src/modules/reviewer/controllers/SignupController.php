<?php

namespace app\modules\reviewer\controllers;

use app\models\Reviewer;
use app\models\User;
use app\modules\reviewer\components\BaseController;
use app\modules\reviewer\models\SignUpForm;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\ServerErrorHttpException;
use yii\widgets\ActiveForm;

class SignupController extends BaseController
{
    public $layout = 'noauth';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return ArrayHelper::merge($behaviors, [
            'access' => [
                'rules' => [
                    [
                        'actions' => ['index', 'send-code', 'complete', 'activate'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'send-code' => ['post'],
                    'activate' => ['get'],
                ],
            ],
        ]);
    }

    public function actionSendCode()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $result = [
            'errors' => [],
            'status' => 0,
            'codeRefId' => null,
        ];

        $model = new SignUpForm(['scenario' => SignUpForm::SCENARIO_STEP1]);
        $model->attributes = \Yii::$app->request->post();

        if (!$model->validate()) {
            $result['errors'] = $model->getErrors();
            $result['status'] = 1;
            return $result;
        }

        /* @todo Send SMS to phone number */
        sleep(3);

        $result['codeRefId'] = md5('4444');
        return $result;
    }

    public function actionComplete()
    {
        return $this->render('complete');
    }

    public function actionIndex()
    {

        $model = new SignUpForm(['scenario' => SignUpForm::SCENARIO_STEP2]);

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $user = new User();
            $user->email = $model->email;
            $user->type = User::TYPE_REVIEWER;
            $user->status = User::STATUS_REGISTERED;
            $user->password_hash = \Yii::$app->security->generatePasswordHash($model->password);

            if (!$user->save()) {
                throw new ServerErrorHttpException('Can not create user.');
            }

            /* Create Reviewer */
            $reviewer = new Reviewer();
            $reviewer->user_id = $user->id;
            $reviewer->amazon_profile_url = $model->amazon_profile_url;
            $reviewer->phone = '+1' . preg_replace('#[^\d]#', '', $model->phone);

            if (!$reviewer->save()) {
                throw new ServerErrorHttpException('Can not create reviewer profile.');
            }

            if (!$this->sendActivationEmail($user)) {
                throw new ServerErrorHttpException('Can not send email message');
            }
            return $this->redirect(\Yii::$app->urlManager->createUrl('/reviewer/signup/complete'));
        }

        return $this->render('index', [
            'model' => $model,
        ]);

    }

    public function actionActivate($key)
    {
        /**
         * @var User $user
         */
        $user = User::find()->where('activation_token = :key', [':key' => $key])->one();

        if (!$user) {
            return $this->render('activation-failed', ['message' => 'Incorrect activation key']);
        }
        $user->activated_at = time();
        $user->status = User::STATUS_ACTIVE;
        if (!$user->save()) {
            return $this->render('activation-failed', ['message' => 'For technical reasons, we can not activate your account right now. Please try later.']);
        }
        \Yii::$app->session->addFlash('success', 'Your account successfully activated.');
        return $this->redirect('/reviewer/auth/login');
    }

    /**
     * @param User $user
     * @return bool
     * @throws InvalidConfigException
     */
    protected function sendActivationEmail($user)
    {
        if (!isset(\Yii::$app->params['emailTemplates'])) {
            throw new InvalidConfigException('Email templates not configured');
        }

        if (!isset(\Yii::$app->params['emailTemplates']['reviewer_activation_email'])) {
            throw new InvalidConfigException('Email template not found');
        }

        $template = \Yii::$app->params['emailTemplates']['reviewer_activation_email'];
        $message = \Yii::$app->mailer->compose($template['view'], [
                'activationUrl' => \Yii::$app->urlManager->createAbsoluteUrl(['/reviewer/signup/activate', 'key' => $user->activation_token]),
            ]
        );
        $message->setSubject($template['subject']);
        $message->setTo($user->email);
        return $message->send();
    }
}