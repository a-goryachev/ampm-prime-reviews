<?php

namespace app\modules\reviewer\components;

use app\models\User;
use yii\filters\AccessControl;
use yii\web\Controller as Base;

class BaseController extends Base
{
    public $enableCsrfValidation = true;

    public $layout = 'main';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors ['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        return !\Yii::$app->user->isGuest && \Yii::$app->user->identity->type === User::TYPE_REVIEWER;
                    }
                ],
            ],
        ];
        return $behaviors;
    }
}