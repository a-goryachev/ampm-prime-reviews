$(function () {

    function enableStep2() {
        $('.step1').css('opacity', 0.4);
        $('.step1 input').prop('readonly', true);
        $('.step1 button').prop('disabled', true);
        $('.step2').css('opacity', 1);
        $('.step2 input').prop('readonly', false);
        $('.step2 button').prop('disabled', false);
    }

    function disableStep2() {
        $('.step2').css('opacity', 0.4);
        $('.step2 input').prop('readonly', true);
        $('.step2 button').prop('disabled', true);
        $('.step1').css('opacity', 1);
        $('.step1 input').prop('readonly', false);
        $('.step1 button').prop('disabled', false);
    }

    $('button.action-send-code').on('click', function (event) {
        var $form = $(this).closest('form')
        var $phone = $form.find('input[name$="[phone]"]')
        $form.yiiActiveForm('validateAttribute', $phone.attr('id'));
        $.ajax({
            url: '/reviewer/signup/send-code',
            method: 'POST',
            data: {
                phone: $phone.val()
            },
            beforeSend: function () {
                $form.find('.process-indicator span').addClass('hidden')
                $form.find('.process-indicator span.spinner').removeClass('hidden')
            },
            success: function (data) {
                if (data.status === 0) {
                    $form.find('input[name$="[codeRefId]"]').val(data.codeRefId)
                    $form.find('.process-indicator span').addClass('hidden')
                    $form.find('.process-indicator span.success').removeClass('hidden')
                    enableStep2()
                    $form.find('input[name$="[code]"]').focus()
                    return
                }
                $form.find('.process-indicator span').addClass('hidden')
                $form.find('.process-indicator span.error').removeClass('hidden')
            },
            error: function (data) {
                $form.find('.process-indicator span').addClass('hidden')
                $form.find('.process-indicator span.error').removeClass('hidden')
                toastr.error("For technical reasons, we can not send you SMS. Please try later.", "System error")
            }
        })
    })

    disableStep2()
})