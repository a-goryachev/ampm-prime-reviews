<?php
namespace app\modules\reviewer\assets;

use yii\web\AssetBundle;
use yii\web\View;

class SignupAsset extends AssetBundle
{
    public $sourcePath = '';
    public $css = [];
    public $js = [
        'signup.js',
    ];
    public $jsOptions = [
        'position' => View::POS_END
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public function init()
    {
        $this->sourcePath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'signup';
    }
}