<?php
namespace app\modules\reviewer\models;

use yii\base\Model;

/**
 * Class PasswordRecoverySetPasswordForm
 * @package app\modules\reviewer\models
 */
class PasswordRecoverySetPasswordForm extends Model
{
    public $password;
    public $password_repeat;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['password', 'password_repeat'], 'required'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Password must be repeated exactly.']
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'New password',
            'password_repeat' => 'New password again',
        ];
    }
}