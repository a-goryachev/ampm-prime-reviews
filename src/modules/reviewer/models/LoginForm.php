<?php
namespace app\modules\reviewer\models;

use app\models\LoginForm as Base;
use app\models\User;

class LoginForm extends Base
{
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        $_user = parent::getUser();

        if (!$_user || $_user->type !== User::TYPE_REVIEWER) {
            $this->_user = null;
        }

        return $this->_user;
    }
}