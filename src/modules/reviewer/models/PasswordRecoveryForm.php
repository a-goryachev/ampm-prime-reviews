<?php
namespace app\modules\reviewer\models;

use yii\base\Model;

/**
 * Class PasswordRecoveryForm
 * @package app\modules\reviewer\models
 */
class PasswordRecoveryForm extends Model
{
    public $email;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            ['email', 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
        ];
    }
}