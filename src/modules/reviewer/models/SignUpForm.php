<?php
namespace app\modules\reviewer\models;

use yii\base\Model;

/**
 * Class SignUpForm
 * @package app\modules\reviewer\models
 */
class SignUpForm extends Model
{
    const SCENARIO_STEP1 = 'step1';
    const SCENARIO_STEP2 = 'step2';

    public $phone;
    public $code;
    public $codeRefId;
    public $email;
    public $password;
    public $password_repeat;
    public $amazon_profile_url;
    public $agree = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['phone'], 'required', 'on' => self::SCENARIO_STEP1],
            [['phone', 'code', 'codeRefId', 'email', 'password', 'password_repeat', 'amazon_profile_url'], 'required', 'on' => self::SCENARIO_STEP2],
            ['phone', 'validatePhone', 'on' => self::SCENARIO_STEP1],
            ['phone', 'validatePhone', 'on' => self::SCENARIO_STEP2],
            ['code', 'validateCode', 'on' => self::SCENARIO_STEP2],
            ['email', 'email', 'on' => self::SCENARIO_STEP2],
            ['email', 'unique', 'targetClass' => 'app\models\User', 'targetAttribute' => 'email', 'on' => self::SCENARIO_STEP2],
            ['agree', 'boolean', 'on' => self::SCENARIO_STEP2],
            ['agree', 'validateAgree', 'on' => self::SCENARIO_STEP2],
            ['amazon_profile_url', 'validateAmazonProfileUrl', 'on' => self::SCENARIO_STEP2],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'on' => self::SCENARIO_STEP2, 'message' => 'Password must be repeated exactly.']
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Phone number',
            'code' => 'Verification code',
            'email' => 'Email',
            'agree' => 'I agree with Terms and Conditions',
            'password' => 'Password',
            'password_repeat' => 'Password confirmation',
            'amazon_profile_url' => 'Amazon profile URL'
        ];
    }

    public function validateCode($attribute, $params)
    {
        /* @todo Create true code validation by codeRefId value */
        if ($this->code !== '4444') {
            $this->addError('code', 'Incorrect code');
        }
    }

    public function validateAgree($attribute, $params)
    {
        if (!$this->agree) {
            $this->addError('agree', 'You must accept Terms and Conditions');
        }
    }

    public function validateAmazonProfileUrl($attribute, $params)
    {
        $test = trim($this->amazon_profile_url);
        if (!preg_match('#^https?://www.amazon.com/gp/profile/[A-Z0-9]+.*#i', $test)) {
            $this->addError('amazon_profile_url', 'Incorrect Amazon profile URL.');
        }
    }

    public function validatePhone($attribute, $params)
    {
        $phone = preg_replace('#[^\d]#', '', $this->phone);
        if (strlen($phone) !== 10) {
            $this->addError($attribute, 'Phone number is incorrect');
        }
    }

}