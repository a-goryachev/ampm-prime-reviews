Prime Reviews Web Application
============================

Application based on Yii2 PHP Framework as backend part and AngularJS applications as frontend part.


DIRECTORY STRUCTURE
-------------------

      .automation/        contains repository webhook handlers 
      .design/            contains ready HTML/CSS/JS theme to use as UI 
      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      frontend/           contains web applications for reviewer and seller private part, also public part
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources
