<?php

/* @var $this \yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="error-container">
    <i class="pe-7s-way text-success big-icon"></i>

    <h1><?= Html::encode($this->title) ?></h1>
    <strong><?= nl2br(Html::encode($message)) ?></strong>

    <p>
        The server encountered something unexpected that didn't allow it to complete the request. We apologize.
    </p>
    <div>
        <a href="<?= \Yii::$app->urlManager->createUrl('/') ?>" class="btn btn-success">Back to main page</a>
    </div>

</div>
