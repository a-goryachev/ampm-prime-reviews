<?php
use app\assets\AppAsset;
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 */
$assets = AppAsset::register($this);
?>
<?= $this->beginPage() ?>
    <!DOCTYPE html>
    <html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?= Html::csrfMetaTags() ?>

        <!-- Page title -->
        <title><?= Html::encode($this->title) ?></title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <?php $this->head() ?>
    </head>
    <body class="<?= isset($this->params['bodyClass']) ?$this->params['bodyClass']: 'blank' ?>">
    <?php $this->beginBody() ?>

    <!-- Simple splash screen-->
    <div class="splash">
        <div class="color-line"></div>
        <div class="splash-title"><h1>Prime Reviews</h1>

            <p>The best way to bind sellers and reviewers.</p><img src="<?= $assets->baseUrl ?>/images/loading-bars.svg"
                                                                   width="64" height="64"/></div>
    </div>
    <!--[if lt IE 7]>
    <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a
        href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?= $content ?>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>