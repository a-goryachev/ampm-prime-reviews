<?php

use yii\db\Migration;
use yii\db\pgsql\Schema;

class m160105_101829_mod_ReviewerFinderUser_add_token_and_subscription_status extends Migration
{
    protected $sTableName = '{{ReviewerFinderUser}}';

    public function safeUp()
    {
        $this->addColumn($this->sTableName, 'token', Schema::TYPE_STRING . '(128)');
        $this->createIndex('token', $this->sTableName, 'token');
        $this->addColumn($this->sTableName, 'ac_subscription_status', Schema::TYPE_INTEGER . '(1) NOT NULL DEFAULT 0');
        $this->createIndex('ac_subscription_status', $this->sTableName, 'ac_subscription_status');
        $this->addColumn($this->sTableName, 'ac_subscriber_id', Schema::TYPE_INTEGER);
        $this->createIndex('ac_subscriber_id', $this->sTableName, 'ac_subscriber_id');
    }

    public function safeDown()
    {
        $this->dropColumn($this->sTableName, 'ac_subscriber_id');
        $this->dropColumn($this->sTableName, 'ac_subscription_status');
        $this->dropColumn($this->sTableName, 'token');
    }
}
