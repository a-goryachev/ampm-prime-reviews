<?php

use yii\db\Migration;
use yii\db\pgsql\Schema;

class m151213_143641_add_Reviewer extends Migration
{
    protected $sTableName = '{{Reviewer}}';

    public function safeUp()
    {
        $this->createTable($this->sTableName, [
            'id' => 'pk',
            'amazon_profile_url' => Schema::TYPE_STRING . '(500) NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status' => Schema::TYPE_INTEGER . '(1) NOT NULL DEFAULT 0'
        ]);
        $this->addForeignKey('fk_user_id', $this->sTableName, 'user_id', '{{User}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable($this->sTableName);
    }
}
