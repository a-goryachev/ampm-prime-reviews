<?php

use yii\db\Migration;
use yii\db\pgsql\Schema;

class m151213_100437_add_User extends Migration
{
    protected $sTableName = '{{User}}';

    public function safeUp()
    {
        $this->createTable($this->sTableName, [
            'id' => 'pk',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'type' => Schema::TYPE_INTEGER . '(1) NOT NULL',
            'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
            'activation_token' => Schema::TYPE_STRING . ' NOT NULL',
            'access_token' => Schema::TYPE_STRING . ' NOT NULL',
            'access_token_expires' => Schema::TYPE_INTEGER . ' NOT NULL',
            'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
            'password_reset_token' => Schema::TYPE_STRING,
            'status' => Schema::TYPE_INTEGER . '(1) NOT NULL DEFAULT 0',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'activated_at' => Schema::TYPE_INTEGER,
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->sTableName);
    }
}
