<?php

use yii\db\Migration;
use yii\db\pgsql\Schema;

class m151215_153100_mod_Reviewer_add_phone extends Migration
{
    protected $sTableName = '{{Reviewer}}';

    public function safeUp()
    {
        $this->addColumn($this->sTableName, 'phone', Schema::TYPE_STRING . '(12) NOT NULL');
        $this->createIndex('phone', $this->sTableName, 'phone');
    }

    public function safeDown()
    {
        $this->dropColumn($this->sTableName, 'phone');
    }
}
