<?php

use yii\db\Migration;
use yii\db\pgsql\Schema;

class m151231_073559_add_ReviewerFinderUser extends Migration
{
    protected $sTableName = '{{ReviewerFinderUser}}';

    public function safeUp()
    {
        $this->createTable($this->sTableName, [
            'id' => 'pk',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'activationCode' => Schema::TYPE_STRING . '(6)',
            'numberOfActivations' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'created_at' => Schema::TYPE_INTEGER
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->sTableName);
    }
}
