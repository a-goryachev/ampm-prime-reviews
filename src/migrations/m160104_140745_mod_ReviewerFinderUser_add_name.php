<?php

use yii\db\Migration;
use yii\db\pgsql\Schema;

class m160104_140745_mod_ReviewerFinderUser_add_name extends Migration
{
    protected $sTableName = '{{ReviewerFinderUser}}';

    public function safeUp()
    {
        $this->addColumn($this->sTableName, 'name', Schema::TYPE_STRING);
    }

    public function safeDown()
    {
        $this->dropColumn($this->sTableName, 'name');
    }
}
