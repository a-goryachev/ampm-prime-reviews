<?php
/**
 * This view is used by app/commands/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */

echo "<?php\n";

$action = '';
$table = '';
if (preg_match('#^m\d{6}_\d{6}_(add|mod)_([^_]+).*$#', $className, $matches)) {
    $action = $matches[1];
    $table = $matches[2];
}
?>

use yii\db\pgsql\Schema;
use yii\db\Migration;

class <?= $className ?> extends Migration
{
<?= !empty($table) ? "    protected \$sTableName = '{{{$table}}}';\n" : '' ?>
public function safeUp()
{
<?php if ($action === 'add'): ?>
    $this->createTable($this->sTableName,[
    'id' => 'pk',
    ]);
<?php endif; ?>
}

public function safeDown()
{
<?php if ($action === 'add'): ?>
    $this->dropTable($this->sTableName);
<?php endif; ?>
}
}
