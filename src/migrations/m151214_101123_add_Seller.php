<?php

use yii\db\Migration;
use yii\db\pgsql\Schema;

class m151214_101123_add_Seller extends Migration
{
    protected $sTableName = '{{Seller}}';

    public function safeUp()
    {
        $this->createTable($this->sTableName, [
            'id' => 'pk',
            'full_name' => Schema::TYPE_STRING . '(500) NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status' => Schema::TYPE_INTEGER . '(1) NOT NULL DEFAULT 0'
        ]);
        $this->addForeignKey('fk_user_id', $this->sTableName, 'user_id', '{{User}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable($this->sTableName);
    }
}
