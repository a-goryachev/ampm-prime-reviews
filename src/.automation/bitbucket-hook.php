<?php

class BitbucketDeploy
{
    protected $logFile = '/logs/bitbucket-hook.log';
    protected $hookScript = '/code/.automation/bitbucket-hook.sh';
    protected $repositoryName = 'ampm-prime-reviews';

    public function doDeploy()
    {
        $log = "";
        if (is_readable($this->logFile)) {
            $log = file_get_contents($this->logFile);
        }
        $log .= date("d.m.Y H:i:s") . "\r\n------------------------\r\n";

        if ($_SERVER['REQUEST_METHOD']==='POST') {
            $data = json_decode(file_get_contents('php://input'), true);
            if ($data['repository']['name'] == $this->repositoryName) {
                $res = -512;
                $out=[];
                exec($this->hookScript, $out, $res);
                $out = implode("\r\n", $out);
                $log .= "HOOK SCRIPT OUTPUT:"
                    . "\r\n=======================\r\n"
                    . $out
                    . "\r\n=======================\r\n";
            } else {
                $log .= "Bad repository name";
            }
        } else {
            $log .= "Invalid request";
        }

        $log .= "\r\n------------------------\r\n";
        file_put_contents($this->logFile, $log);
    }
}

$deploy = new BitbucketDeploy();
$deploy->doDeploy();