#!/bin/sh

echo "start post-receive hook"
cd /app
CUR_REV=`git rev-parse HEAD`
git pull
COMPOSER_CHANGED=`git diff --name-only $CUR_REV HEAD | grep composer.json`
cd /code
[ "$COMPOSER_CHANGED" = "" ] || composer -v update
#echo "yes" | php ./tests/codeception/bin/yii migrate
echo "yes" | php ./yii migrate
echo "removing assets..."
find web/assets/* -type d -exec rm -fR {} +
echo "end post-receive hook"
