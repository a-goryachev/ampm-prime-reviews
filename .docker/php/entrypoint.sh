#!/bin/bash

APP_ROOT=/code
[ -d $APP_ROOT ] || (mkdir -p $APP_ROOT)
[ "$(ls -A $APP_ROOT)" ] || (/bin/composer create-project --prefer-dist yiisoft/yii2-app-basic $APP_ROOT)
[ -d $APP_ROOT/vendor ] || (mkdir -p $APP_ROOT/vendor)
[ -f $APP_ROOT/composer.json ] && ([ -f $APP_ROOT/composer.lock ] || /bin/composer -v install)
[ -a $APP_ROOT/init ] && ([ -d $APP_ROOT/web ] && ([ -f $APP_ROOT/web/index.php ] || (php ./init --env=Development --overwrite=y)))
[ -d $APP_ROOT ] &&(chown -R 1000:1000 $APP_ROOT)
/usr/sbin/php5-fpm -R
