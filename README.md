Prime Reviews
=============
Website to allow Amazon's sellers find reviewers for their products.
Application based on Yii2 PHP Framework as backend part and AngularJS applications as frontend part.


DIRECTORY STRUCTURE
-------------------

      .docker/            contains Dockerfiles for all used containers 
      src/                contains source code


REQUIREMENTS
------------

- Linux based operating system
- Docker engine
- Docker compose
- Git

INSTALLATION
------------

- Clone project from repository: ```git clone git@bitbucket.org:a-goryachev/prime-reviews.git app```
- Go to project folder: ```cd app```
- Copy docker-compose.yml into docker-compose.local.yml ```cp docker-compose.yml docker-compose.local.yml``` 
- Edit docker-compose.local.yml to configure environment (ports, restart policy)
- Build container images: ```./docker-compose build```
- Start containers: ```./docker-compose up```
- Press ```Ctrl+C``` to stop containers then start containers again in background: ```./docker-compose start```
- Check main page opens in your browser

MANAGE CONTAINERS
-----------------

- Show containers: ```./docker-compose ps -a```
- Connect to container: ```docker exec -it <container_name> <command>```
    - PHP container: ```docker exec -it ampm_prime_reviews_php /bin/bash```
    - PostgreSQL container: ```docker exec -it ampm_prime_reviews_pgsql psql -U dba```
- Rebuild containers after changes in ```.docker``` folder: ```./docker-compose build```
- Recreate containers (if needed) and start: ```./docker-compose up -d```

AUTOMATION
----------

You can configure your Bitbucket GIT repository to use webhooks when someone push changes. Application can handle webhooks and updates source code, apply migrations etc.

To configure repository create webhook with ```URL=<your domain>/b/hooks/bitbucket-hook.php```. Then push to repository and check logs/bitbucket-hook.log to see results.
